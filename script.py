import argparse
import socket
import concurrent.futures

from scapy.all import sr1 as send_recv
from scapy.layers.inet import IP, ICMP, TCP


def host_exists(ip_addr):
    packet = IP(dst=ip_addr) / ICMP()
    result = send_recv(packet, timeout=2, verbose=0)
    return result is not None


def is_open(ip_addr, port_no):
    syn = IP(dst=ip_addr) / TCP(dport=port_no, flags='S')
    syn_ack = send_recv(syn, timeout=2, verbose=0)
    try:
        flags = syn_ack.getlayer(TCP).flags
        return flags == 'SA'
    except AttributeError:
        return False


def subproc(ip_addr, port_no):
    port_open, application = is_open(ip_addr, port_no), socket.getservbyport(port_no) or 'unknown'
    if port_open:
        desc = 'designated' if port_no < 1024 else 'usually used'
        print('Port {} is open. The port is {} for "{}".'.format(port_no, desc, application.upper()))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Stealthy SYN Scan')
    parser.add_argument('ip', type=str, help='IP Address')
    parser.add_argument('-p', '--port-range', type=int, nargs=2, default=(1, 65535))
    parser.add_argument('-w', '--workers', type=int, default=64, help='No. of parallel workers.')
    args = parser.parse_args()

    if not host_exists(args.ip):
        print('Could not contact the host.')

    pool = concurrent.futures.ProcessPoolExecutor(max_workers=args.workers)
    print('Scanning ports...')
    for port in range(*args.port_range):
        pool.submit(subproc, args.ip, port)

    pool.shutdown(wait=True)
    print('Scan is finished.')
